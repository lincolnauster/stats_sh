# wasm dockerfile
FROM emscripten/emsdk
RUN echo 'deb http://deb.debian.org/debian unstable main' >> /etc/apt/sources.list
RUN apt update --allow-insecure-repositories
RUN yes | apt install bash -y --allow-unauthenticated

RUN curl https://wasmtime.dev/install.sh -sSf | bash
RUN ln -sf $HOME/.bashrc $HOME/.bash_profile
