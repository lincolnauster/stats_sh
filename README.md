# stats_sh
fast and static wasm statistical calculator

![A screenshot of the website](./sc.png)

## Building, Testing
Building requires:
* emsdk (to provide emcc)
* Bash >= 5.1

`./build.sh` will generate the static site in a newly created target/ directory.
Serve that with any web server (usually `python3 -m http.server` for testing).

Running the unit tests require the `wasmtime` runtime environment. With that
installed, `test.sh` will log the results of running the tests, and exit with 0
on success, and 1 on the failure of at least one test.

## Motivation
The goal of `stats_sh` is to provide a consistent, thorough, and fast platform
for running statistical tests. In my class, the calculators that were used
were all some varying combination of slow to compute, difficult to navigate,
and difficult to use. I believe these can be addressed with a combination of
modern web technologies, such as Web Assembly, and simplicitly lacking in many
online platforms such as those used in my class. Therefore, stats_sh produces
a static site built from a combination of input and output templates, running
web assembly for complex computations.

## Architecture
This repository, rather than being a calculator in and of itself, serves to
generate a usable calculator. It operates in a somewhat declarative manner,
where tests, defined in `src/` and declared in header files, are parsed for
their inputs and outputs. These inputs and outputs are collected and rendered to
HTML by the build script, utilizing templates in `templates/`. These test pages
are then linked to from the similarly generated index. A C file imperatively
defines the steps to run the test, which is compiled to wasm and sourced on the
page, along with a JS script to parse outputted C structs into DOM information.

## Current Completeness State
In terms of semantic versioning, I'd say this is currently in an `0.1.x` stage.
The interface is, for the most part, defined (save future additions and
unexpected modifications), but functionality is nowhere near complete. *Many
more* statistical tests need to be added --- what's currently on the website is
strongly in proof-of-concept territory, and not all of them are implemented yet.
