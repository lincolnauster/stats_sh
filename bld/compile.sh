#!/usr/bin/env bash

compile() {
    OUT="$(emcc "src/$1.c" src/types.c src/type_ops.c -o "target/res/$1.js" \
        --no-entry -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall','cwrap']" -Os \
        2>&1)"

    if [[ $? != '0' ]]; then
        log_warning "Error when running emcc over: src/$1.c"
    else
        log_info "compiled $1"
    fi
    
    if [[ $LOUD_COMPILE == '1' ]]; then
        echo "$OUT"
    fi
}
