#!/usr/bin/env bash
# Load config from ENV_VARS

if [[ "$1" = '--help' ]]; then
    echo "* build.sh - compile a static site from source and templates *"
    echo ""
    echo "  - To build the site for stats.sh, run with no arguments or    "
    echo "    environment variables set."
    echo "  - To add a new test, look at the src/ directory and its associated"
    echo "    documentation. If you do everything correctly, build.sh will pick"
    echo "    it up and index it with the rest of the tests."
    echo ""
    echo "* configuring build.sh *"
    echo ""
    echo "  - build.sh's behavior may be modified with environment variables."
    echo "    Setting an environment variable to anything but 0 nables it."
    echo ""
    echo "  - TIMED: Time each portion of the generation process."
    echo "  - SUPRESS_WARNINGS: Don't show warnings."
    echo "  - LOUD_COMPILE: Show compiler output."
    exit 0
fi

if [[ -z "$TIMED" ]]; then
    CONF_TIMED='0'
else
    CONF_TIMED='1'
fi

if [[ -z "$SUPRESS_WARNINGS" ]]; then
    SUPRESS_WARNINGS='0'
else
    SUPRESS_WARNINGS='1'
fi

if [[ -z "$LOUD_COMPILE" ]]; then
    LOUD_COMPILE='0'
else
    LOUD_COMPILE='1'
fi

get_conf() {
    echo "TIMED: $CONF_TIMED"
    echo "SUPRESS_WARNINGS: $SUPRESS_WARNINGS"
    echo "LOUD_COMPILE: $LOUD_COMPILE"
}
