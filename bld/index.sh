#!/usr/bin/env bash
# This shell script is responsible for building index.html, from both normal
# 'templating engine' type functions and parsing the backend.

TEST_NAV_INDICATOR='TESTS'
TEST_NAME_INDICATOR='NAME'
TEST_CATEGORY_INDICATOR='CATEGORY'
DATA_ENTRY_INDICATOR='DATA ENTRY'

INDEX_HTML_LOCATION='target/index.html'
INDEX_TEMPLATE_LOCATION='templates/index.html'
TEST_TEMPLATE_LOCATION='templates/test.html'
HEADER_TEMPLATE_LOCATION='templates/header.html'

build_index() {
    cp "$INDEX_TEMPLATE_LOCATION" "$INDEX_HTML_LOCATION"
    build_header "$INDEX_HTML_LOCATION"
}

add_test() {
    test="$(cat $TEST_TEMPLATE_LOCATION)"
    name="$(name_in_test "$1")"
    category="$(category_in_test "$1")"
    test="$(replace_template_stdin "$TEST_NAME_INDICATOR" "$name" <<< "$test")"
    test="$(replace_template_stdin "$TEST_CATEGORY_INDICATOR" "$category" <<< "$test")"
    test+=$'\n'
    test+="$TEMPLATE_SURROUNDING$TEST_NAV_INDICATOR$TEMPLATE_SURROUNDING"

    replace_template "$TEST_NAV_INDICATOR" "$test" "$INDEX_HTML_LOCATION"
}

finish_tests() {
    replace_template "$TEST_NAV_INDICATOR" "" "$INDEX_HTML_LOCATION"
}
