#!/usr/bin/env bash

HEADER_INDICATOR='HEADER'

replace_template() {
    sed -i \
"s/$TEMPLATE_SURROUNDING$1$TEMPLATE_SURROUNDING/\
$(escape_html $2)/g" "$3"
}

replace_template_stdin() {
    sed \
"s/$TEMPLATE_SURROUNDING$1$TEMPLATE_SURROUNDING/\
$(escape_html $2)/g"
}

build_header() {
    header="$(cat "$HEADER_TEMPLATE_LOCATION")"
    replace_template "$HEADER_INDICATOR" "$header" "$1"
}
