#!/usr/bin/env bash

TEST_PAGE_TEMPLATE_LOCATION='templates/test_page.html'
TEST_DESCRIPTION_INDICATOR='DESCRIPTION'
TEST_FORMULA_INDICATOR='FORMULA'
TEST_OUTPUT_INDICATOR='OUTPUT'
TEST_INPUT_INDICATOR='INPUT'
JS_INDICATOR='JS_NAME'

# format $@ as a failure, with $1 being a summary and all but the first as a
# full explanation of the error.
_user_facing_failure() {
    echo "<br/><i>$1</i><br/>$@"
}

new_test_page() {
    location="target/$(name_in_test "$1").html"
    cp "$TEST_PAGE_TEMPLATE_LOCATION" "$location"
    
    replace_template "$TEST_NAME_INDICATOR" "$(name_in_test "$1")" "$location"
    replace_template "$TEST_CATEGORY_INDICATOR" \
		     "$(category_in_test "$1")" "$location"
    
    replace_template "$TEST_DESCRIPTION_INDICATOR" \
		     "$(description_in_test "$1")" "$location"
    
    replace_template "$TEST_FORMULA_INDICATOR"\
		     "$(formula_in_test "$1")" "$location"
    
    replace_template "$JS_INDICATOR"\
		     "$(js_name_in_test "$1")" "$location"

    input_name="$(input_format_in_test "$1")"
    input_path="templates/inputs/$input_name.html"
    in="$(cat "$input_path" 2> /dev/null)"
    
    if [[ $? != '0' ]]; then
        log_warning "Couldn't find template for input \`$input_name\`"
        replace_template "$TEST_INPUT_INDICATOR" \
            _user_facing_error \
	        "invalid input type" \
	        "If you're an end user, report this as a bug :)" \
        "$location"
    else
        replace_template "$TEST_INPUT_INDICATOR" "$in" "$location"
    fi

    output_name="$(output_format_in_test "$1")"
    output_path="templates/outputs/$output_name.html"
    out="$(cat "$output_path" 2> /dev/null)"
    
    if [[ $? != '0' ]]; then
        log_warning "Couldn't find output template for \`$output_name\`"
        replace_template "$TEST_OUTPUT_INDICATOR" \
	    _user_facing_error \
	        "invalid output type" \
	        "If you're an end user, report this as a bug :)" \
        "$location"
    else
        replace_template "$TEST_OUTPUT_INDICATOR" "$out" "$location"
    fi

    build_header "$location"
}
