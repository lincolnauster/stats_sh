#!/usr/bin/env bash
shopt -s nullglob

TEMPLATE_SURROUNDING='!'
DELIM_CHARACTER='#' # dividing serializations of data

. ./bld/compile.sh
. ./bld/template_util.sh
. ./bld/test_pages.sh
. ./bld/index.sh

log_toplevel() {
    printf "\033[1;32m%s\033[0m\n" "$*" 1>&2
}

log_info() {
    printf "\033[0;33m%s\033[0m\n" "$*" 1>&2
}

log_warning() {
    if [[ $SUPRESS_WARNINGS != '1' ]]; then
        printf "\033[0;33mWARNING: %s\033[0m\n" "$*" 1>&2
    fi
}

# Escape html (potentially for sed)
# i.e., <h1>title</h1> becomes <h1>title<\/h1>
escape_html() {
    sed -E 's/(\/|\&)/\\\1/g' <<< "$@"
}

# Create the target directory if it doesn't exist. Returns 1 if target doesn't
# exist and was created, 0 if nothing happened.
make_target() {
    if [ ! -d './target' ]; then
        mkdir ./target
		return 1
    fi
	return 0
}

# Find and index all the tests, then call $@ on them.
_for_tests() {
    for f in src/*.h; do
        name="$(_find_metadata_in_func 'NAME' "$f")"
        category="$(_find_metadata_in_func 'CATEGORY' "$f")"
        description="$(_find_metadata_in_func 'DESCRIPTION' "$f")"
        formula="$(_find_metadata_in_func 'FORMULA' "$f")"
        formula="$(sed 's/\\/\\\\/g' <<< "$formula")"

        func_name="$(sed 's/[ -]/_/g' <<< "${name@L}")"
        func="$(grep -E '^[^ ]+ \*?'"$func_name" "$f")"
        output_format="$(sed -E 's/(.) \*?'"$func_name"'.*/\1/g' <<< "$func")"


        if [[ -z "$name" || -z "$category" || -z "$output_format" ]]; then
            log_warning "Header file $f didn't contain necessary information."
            continue
        fi

        input_format="$(sed -E \
            's/'"$output_format"' '"$func_name\((const |)(.+) \**\);"'/\2/g' \
            <<< "$func")"

        if [ -z "$name" ]; then
            name="$(basename "$f" .h)"
        fi

        if [[ "$name$category$description$formula" =~ .*"$DELIM_CHARACTER".* ]]; then
            tmp="Metadata cannot contain the delimiter character "
            tmp+="\`$DELIM_CHARACTER\`."
            log_warning "$tmp"
            log_warning "Ignoring $name."
            continue
        fi

        $@ "$name" "$category" "$description" "$formula" "$output_format" \
           "$input_format" "$(basename "${f%.*}")"
    done
}

# Serialize a test, given _serialize_test $name $category $description
# $formula $output_format, return as string with the form:
# category|name|description|formula|output_format|input_format|file_name
_serialize_test() {
    printf '%s%s%s%s%s%s%s%s%s%s%s%s%s\n' \
        "$2" "$DELIM_CHARACTER" \
        "$1" "$DELIM_CHARACTER" \
        "$3" "$DELIM_CHARACTER" \
        "$4" "$DELIM_CHARACTER" \
        "$5" "$DELIM_CHARACTER" \
        "$6" "$DELIM_CHARACTER" \
        "$7"
}

name_in_test() {
    cut -d "$DELIM_CHARACTER" -f 2 <<< "$1"
}

category_in_test() {
    cut -d "$DELIM_CHARACTER" -f 1 <<< "$1"
}

description_in_test() {
    cut -d "$DELIM_CHARACTER" -f 3 <<< "$1"
}

formula_in_test() {
    cut -d "$DELIM_CHARACTER" -f 4 <<< "$1"
}

output_format_in_test() {
    cut -d "$DELIM_CHARACTER" -f 5 <<< "$1"
}

input_format_in_test() {
    cut -d "$DELIM_CHARACTER" -f 6 <<< "$1"
}

file_name_in_test() {
    cut -d "$DELIM_CHARACTER" -f 7 <<< "$1"
}

js_name_in_test() {
    printf '%s.js\n' "$(file_name_in_test "$1")"
}

# Find a category of metadata in a function's docstring.
# `find_metadata_in_func NAME file` will return `abc name` for:
# ```
# file.c
#
# // NAME: abc name
_find_metadata_in_func() {
    sed -r "s/\n?\/\/ $1: (.*)/\1/;t;d" "$2"
}

log_toplevel "loading config"
. ./bld/conf.sh
log_info "$(get_conf)"

log_toplevel "collecting tests"
if [[ $CONF_TIMED = '1' ]]; then
    time tests="$(_for_tests _serialize_test | sort)"
else
    tests="$(_for_tests _serialize_test | sort)"
fi

log_toplevel "setting up \`target/\`"
make_target
if [[ $? != '0' ]]; then
    log_info "created \`target/\`"
fi

log_info "copying \`static/\`"
if [[ $CONF_TIMED = '1' ]]; then
    time cp -TR ./static ./target/
else
    cp -TR ./static ./target/static/
fi

log_toplevel "building index"
build_index

IFS=$'\n'
if [[ $CONF_TIMED = '1' ]]; then
    time for test in $tests; do
        add_test "$test"
    done
else
    for test in $tests; do
        add_test "$test"
    done
fi

finish_tests

log_toplevel "building test pages"
mkdir -p 'target/script'

IFS=$'\n'
if [[ $CONF_TIMED = '1' ]]; then
    time for test in $tests; do
        new_test_page "$test"
        log_info "built $(name_in_test "$test")"
    done
else
    for test in $tests; do
        new_test_page "$test"
        log_info "built $(name_in_test "$test")"
    done
fi

log_info "copying js"
for test in $tests; do
    js_name="$(js_name_in_test "$test")"
    cp "src/$js_name" "target/script/$js_name" 2> /dev/null
done

log_toplevel "compiling test modules"
mkdir -p 'target/res'

if [[ $CONF_TIMED = '1' ]]; then
    time for test in $tests; do
        time compile "$(file_name_in_test "$test")"
    done
else
    for test in $tests; do
        compile "$(file_name_in_test "$test")" &
    done
    wait
fi
