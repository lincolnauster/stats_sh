#!/usr/bin/env bash

if [ ! -d target ]; then
    echo "target/ dir not found - did you build the site?"
    exit 1
fi

type html-minifier > /dev/null
if [[ $? != '0' ]]; then
    echo "html-minifier not found: will not minify"
else
    for f in target/*.html; do
        html-minifier "$f" -o "$f" --collapse-whitespace
    done
fi

tar -cJf "stats_sh-$(date '+%d%m%Y')-release.tar.xz" target/
