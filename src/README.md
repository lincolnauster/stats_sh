# src

Statistical tests are defined here. To build properly, a statistical test
requires at three files:
* test.h: define the API for the test. This holds metadata (a pretty name,
  description, and LaTeX formula) and defines the API of the test, including a
  *single* input structure (pointer or const pointer) and *single* output
  structure (stack allocated).
* test.c: the actual computations of the test
* test.js: load the compiled wasm module (will be res/test.wasm) and perform all
  the associated IO: verify input, set outputs, etc.
