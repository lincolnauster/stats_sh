#include <emscripten.h>

#include "types.h"
#include "type_ops.h"
#include "mean.h"

EMSCRIPTEN_KEEPALIVE double
arithmetic_mean(const List *x)
{
	return mean(x);
}

EMSCRIPTEN_KEEPALIVE int
mean_is_valid(const List *x)
{ return x->len != 0; }

#ifdef TEST
#include <stdlib.h>

int
main()
{
	double *lt1_v = malloc(2 * sizeof(double)); lt1_v[0] = 0.0; lt1_v[1] = 1.0;

	List lt1 = { lt1_v, 2 };
 	List lt2 = { NULL, 0 };

	if (arithmetic_mean(&lt1) != 0.5) return -1;
 	if (!mean_is_valid(&lt1)) return -1;
	if (mean_is_valid(&lt2)) return -1;
	return 0;
}
#endif /* TEST */
