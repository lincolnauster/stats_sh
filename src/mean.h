// CATEGORY: Summary Stats
// NAME: Arithmetic Mean
// FORMULA: \frac{1}{n} \sum_{i=1}^n X_i
double arithmetic_mean(const List *);
int mean_is_valid(const List *);
