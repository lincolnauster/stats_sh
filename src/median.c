#include <emscripten.h>

#include "types.h"
#include "type_ops.h"

#include "mean.h"

EMSCRIPTEN_KEEPALIVE double
median(List *x)
{
	sort(x->vals, 0, x->len);
	int mid = x->len / 2;
	if (x->len % 2 == 0) return (x->vals[mid] + x->vals[mid - 1]) / 2;
	else return x->vals[mid];
}

#ifdef TEST
#include <stdlib.h>

int
main()
{
	double *lt1_v = malloc(3 * sizeof(double));
	lt1_v[0] = 1.0; lt1_v[1] = 2.0; lt1_v[2] = 3.0;

	List lt1 = { lt1_v, 3 };

	if (median(&lt1) != 2.0) return -1;

 	return 0;
}

#endif /* TEST */
