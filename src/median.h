// CATEGORY: Summary Stats
// NAME: Median
// DESCRIPTION: The middle data point (or mean of two middle data points)
double median(const List *);
