Module['onRuntimeInitialized'] = function() {
    let new_list_with_cap = Module.cwrap('new_list_with_cap', 'number', ['number']);
    let list_push = Module.cwrap('list_push', 'number', ['number', 'number']);
    let median = Module.cwrap('median', 'number', ['number']);

    document.getElementsByName('calculate')[0].onclick = function() {
        let elements = document.getElementsByClassName('single_in');

        let l = new_list_with_cap(elements.length);
        for (let i = 0; i < elements.length; i++) {
            list_push(l, parseFloat(elements[i].value));
        }

        document.getElementById("double_out").innerHTML = median(l);
    }
}
