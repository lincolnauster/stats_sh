#include <stdlib.h>

#include <emscripten.h>

#include "types.h"
#include "type_ops.h"
#include "mode.h"

EMSCRIPTEN_KEEPALIVE List *
mode(List *x)
{
	List *modes = new_list_with_cap(x->len / 4 + 1);
	// try to minimize allocations (+ 1 guaruntees at least 1 slot)

	sort(x->vals, 0, x->len);

	double active_mode = x->vals[0];
	list_push(modes, active_mode);

	int count = 1; int mode_count = 1;
	for (int i = 1; i < x->len; ++i) {
		if (x->vals[i] == active_mode) {
			mode_count++;
			continue;
		} else if (x->vals[i] == x->vals[i - 1]) count++;

		if (count == mode_count) {
			active_mode = x->vals[i];
			list_push(modes, active_mode);
		} else if (count > mode_count) {
			active_mode = x->vals[i];
			mode_count = count;
			count = 0;
			modes->len = 0;
			list_push(modes, active_mode);
		}
	}

	return modes;
}

#ifdef TEST
#include <stdio.h>

int
main(void)
{
	double vals[] = { 1.1, 1.1, 1.1, 0.1, 1.1 };
	double vals2[] = { 1.4, 1.1, 0.1, 1.1, 1.1 };
	double vals3[] = { 0.1, 1.1, 0.1, 1.1, 1.1 };
	double vals4[] = { 0.1, 0.1, 1.1, 1.1 };
	double vals5[] = { 1.0, 2.0, 3.0 };
	List l1 = { vals,  5, 5 };
	List l2 = { vals2, 5, 5 };
	List l3 = { vals3, 5, 5 };
	List l4 = { vals4, 4, 4 };
	List l5 = { vals5, 3, 3 };
	if (mode(&l1)->vals[0] != 1.1 && mode(&l1)->len == 1) return 1;
	if (mode(&l2)->vals[0] != 1.1 && mode(&l2)->len == 1) return 1;
	if (mode(&l3)->vals[0] != 1.1 && mode(&l3)->len == 1) return 1;

	if (mode(&l4)->vals[0] != 0.1
	 || mode(&l4)->vals[1] != 1.1
	 || mode(&l4)->len != 2) return 1;

 	printf("%d\n", mode(&l5)->len);
 	if (mode(&l5)->len != 3) return 1;

 	if (l1.len != 5) return 1;
 	if (l2.len != 5) return 1;
 	if (l3.len != 5) return 1;
 	if (l4.len != 4) return 1;
 	if (l5.len != 3) return 1;

	return 0;
}
#endif /* TEST */
