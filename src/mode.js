Module['onRuntimeInitialized'] = function() {
    let new_list_with_cap = Module.cwrap('new_list_with_cap', 'number', ['number']);
    let list_push = Module.cwrap('list_push', 'null', ['number', 'number']);
    let list_pop  = Module.cwrap('list_pop', 'number', ['number']);
    let list_len  = Module.cwrap('list_len', 'number', ['number']);
    let mode = Module.cwrap('mode', 'number', ['number']);

    document.getElementsByName('calculate')[0].onclick = function() {
        let elements = Array.from(document.getElementsByClassName('single_in'));

        Array.from(document.getElementsByClassName('out_num')).slice(1).map(
            element => element.parentNode.removeChild(element)
        );

        let l = new_list_with_cap(elements.length);
        for (let i = 0; i < elements.length; i++) {
            list_push(l, parseFloat(elements[i].value));
        }

        let m = mode(l);
        let m_len = list_len(m);
        let modes = [];

        for (let i = 0; i < m_len; ++i) {
            modes.push(list_pop(m));
        }

        document.getElementsByClassName('out_num')[0].innerHTML = modes[0];
        for (let i = 1; i < m_len; ++i) {
            let a = document.getElementsByClassName('out_num')[0];
            let b = a.cloneNode(true);
            b.innerHTML = modes[i];
            document.getElementById('list_out').appendChild(b);
        }
    }
}
