// NAME: Paired T-Test
// CATEGORY: Paired Data
// DESCRIPTION: The basic case for the comparison of two sample means - requires paired and parametric data.
// FORMULA: \frac{\overline{X}_1 - \overline{X}_2}{s_p\sqrt{\frac{2}{n}}}
double paired_t_test(const PairedList *);
