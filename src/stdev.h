// CATEGORY: Summary Stats
// NAME: Standard Deviation
// FORMULA: \sqrt{s}, \mathrm{where}\,s\,\mathrm{is\,variance}
double standard_deviation(const MarkedList *);
