Module['onRuntimeInitialized'] = function() {
    let new_marked_list_with_cap = Module.cwrap(
        'new_marked_list_with_cap',
        'number',
        ['number']
    );

    let list_push = Module.cwrap('list_push', 'null', ['number', 'number']);
    let marked_list_get_list = Module.cwrap(
        'marked_list_get_list', 'number', ['number']
    );
    let marked_list_set_marked = Module.cwrap(
        'marked_list_set_marked', 'null', ['number']
    );
    let stdev = Module.cwrap('standard_deviation', 'number', ['number']);

    document.getElementsByName('calculate')[0].onclick = function() {
        let elements = document.getElementsByClassName('single_in');

        let ml = new_marked_list_with_cap(elements.length);
        let l = marked_list_get_list(ml);
        marked_list_set_marked(ml, !document.getElementById("sample").checked);

        for (let i = 0; i < elements.length; i++) {
            list_push(l, parseFloat(elements[i].value));
        }

        document.getElementById("double_out").innerHTML = stdev(ml);
    }
}
