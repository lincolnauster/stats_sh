#include <emscripten.h>
#include <math.h>

#include "types.h"
#include "type_ops.h"

void swap(double *x, int a, int b);
void insertion_sort(double *x, int lower, int upper);

double
var(const MarkedList *x)
{
	double u = mean(x->list);
	double sum = 0;
	for (int i = 0; i < x->list->len; ++i) {
		sum += (x->list->vals[i] - u) *
		       (x->list->vals[i] - u);
	}

	return sum / (x->list->len - x->sample);
}

double
mean(const List *x)
{
	double sum = 0;
	for (int i = 0; i < x->len; ++i) sum += x->vals[i];
	return sum / x->len;
}

double
stdev(const MarkedList *x)
{
	return sqrt(var(x));
}

/* quicksort x, with an N = 8 cutoff to insertion. TODO: pseudo-randomize on
 * N > probably like 16 or smth and parallel for large N? */
void
sort(double *x, int lower, int upper)
{
	int n = upper - lower;
	int i = lower;
	int j = upper + 1;
	double pivot;

	if (n <= 1) return;
	if (n <= 8) insertion_sort(x, lower, upper);
	else {
		pivot = x[lower];
		while (1) {
			while (x[++i] < pivot) if (i == upper) break;
			while (x[--j] > pivot) if (j == lower) break;
			if (i >= j) break;
			swap(x, i, j);
		}
		swap(x, lower, j);
		sort(x, lower, j);
		sort(x, j + 1, upper);
	}
}

void
swap(double *x, int a, int b)
{
	double tmp = x[a];
	x[a] = x[b];
	x[b] = tmp;
}

void
insertion_sort(double *x, int lower, int upper)
{
	for (int i = lower; i < upper; ++i)
		for (int j = i; j > 0 && x[j] < x[j - 1]; j--)
			swap(x, j, j - 1);
}
