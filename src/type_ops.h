double mean(const List *);
double var(const MarkedList *);
double stdev(const MarkedList *);
void sort(double *, int, int);

