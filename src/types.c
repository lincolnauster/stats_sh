#include <emscripten.h>
#include <stdlib.h>
#include <stdio.h>

#include "types.h"

EMSCRIPTEN_KEEPALIVE List *
new_list(void)
{
	List *l = malloc(sizeof(List));
	l->vals = NULL;
	l->len = 0;
	l->cap = 0;
	return l;
}

EMSCRIPTEN_KEEPALIVE List *
new_list_with_cap(int c)
{
	List *l = malloc(sizeof(List));
	l->vals = malloc(sizeof(double) * c);
	l->len = 0;
	l->cap = c;
	return l;
}

EMSCRIPTEN_KEEPALIVE void
list_push(List *l, double v)
{
	if (l->len >= l->cap) {
		l->cap += 1;
		l->cap *= 2;
		l->vals = realloc(l->vals, sizeof(double) * l->cap);
	}

	l->vals[(l->len)++] = v;
}

EMSCRIPTEN_KEEPALIVE double
list_pop(List *l)
{
	if (l->len <= 0) return 0.0;

	double v = l->vals[--(l->len)];

	if (l->len * 2 < l->cap) {
		l->cap /= 2;
		l->vals = realloc(l->vals, sizeof(double) * l->cap);
	}

	return v;
}

EMSCRIPTEN_KEEPALIVE int
list_len(const List *l)
{
	return l->len;
}

EMSCRIPTEN_KEEPALIVE MarkedList *
new_marked_list(void)
{
	MarkedList *m = malloc(sizeof(MarkedList));
	m->sample = 1;
	m->list = new_list();
	return m;
}

EMSCRIPTEN_KEEPALIVE MarkedList *
new_marked_list_with_cap(int c)
{
	MarkedList *m = new_marked_list();
	m->list = new_list_with_cap(c);
	return m;
}

EMSCRIPTEN_KEEPALIVE List *
marked_list_get_list(MarkedList *m)
{
	return m->list;
}

EMSCRIPTEN_KEEPALIVE void
marked_list_set_marked(MarkedList *m, int a)
{
	m->sample = a;
}

#ifdef TEST_TYPES
int
main(void)
{
	List *l = new_list_with_cap(8);
	list_push(l, 1);
	list_push(l, 2);
	if (l->cap != 8) return 1;
	for (int i = 0; i < 7; ++i) list_push(l, 3);
	if (l->cap != 18) return 1;
	if (l->vals[0] != 1) return 1;
	if (l->vals[1] != 2) return 1;
	if (l->vals[2] != 3) return 1;
	for (int i = 0; i < 7; ++i) if (list_pop(l) != 3) return 1;
	if (list_pop(l) != 2) return 1;
	if (list_pop(l) != 1) return 1;
	if (list_pop(l) != 0) return 1;

	return 0;
}
#endif /* TEST */
