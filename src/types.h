typedef struct {
    double *vals;
    int len;
    int cap;
} List;

typedef struct {
    List *list;
    int sample;
} MarkedList;

typedef struct {
    List *a;
    List *b;
    int len;
} PairedList;

EMSCRIPTEN_KEEPALIVE List  *new_list(void);
EMSCRIPTEN_KEEPALIVE List  *new_list_with_cap(int c);
EMSCRIPTEN_KEEPALIVE void   list_push(List *, double);
EMSCRIPTEN_KEEPALIVE double list_pop(List *);
EMSCRIPTEN_KEEPALIVE int    list_len(const List *);

EMSCRIPTEN_KEEPALIVE MarkedList *new_marked_list(void);
EMSCRIPTEN_KEEPALIVE MarkedList *new_marked_list_with_cap(int c);
EMSCRIPTEN_KEEPALIVE List *marked_list_get_list(MarkedList *);
EMSCRIPTEN_KEEPALIVE void marked_list_set_marked(MarkedList *, int);
