#include <emscripten.h>

#include "types.h"
#include "type_ops.h"
#include "variance.h"

EMSCRIPTEN_KEEPALIVE double
variance(const MarkedList *x)
{
	return var(x);
}

#ifdef TEST
#include <stdlib.h>

int
main()
{
	double *lt1_v = malloc(2 * sizeof(double));
	lt1_v[0] = 1.0; lt1_v[1] = 2.0;
	double *lt2_v = malloc(6 * sizeof(double));
	lt2_v[0] = 1.0; lt2_v[1] = 2.0; lt2_v[2] = 3.0; lt2_v[3] = 4.0;
	lt2_v[4] = 5.0; lt2_v[5] = 6.0;

	List lt1 = { lt1_v, 2 };
	List lt2 = { lt2_v, 6 };
	MarkedList ml1 = { &lt1, 0 };
	MarkedList ml2 = { &lt1, 1 };
	MarkedList ml3 = { &lt2, 1 };

	if (variance(&ml1) != 0.25) return 1;
	if (variance(&ml2) != 0.5) return 1;
	if (variance(&ml3) != 3.5) return 1;

 	return 0;
}

#endif /* TEST */
