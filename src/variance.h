// CATEGORY: Summary Stats
// NAME: Variance
// DESCRIPTION: A measure of variability
// FORMULA: \sigma^2 = \frac{1}{n}\sum_{i=1}^n\left(X_i - \overline{X}\right)^2
double variance(const MarkedList *);
