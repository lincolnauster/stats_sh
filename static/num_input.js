let state = {
    _n: 1,

    add: function() {
        let last = document.getElementsByClassName("single_in")[this._n - 1];
        last.insertAdjacentHTML('afterend',
           "<input type='number' class='single_in'/>"
        );

        last = document.getElementsByClassName("single_in")[this._n];
        last.addEventListener("keyup", add_input);
        last.focus();

        this._n++;
    },

    remove: function(obj) {
        if (this._n <= 1) { return; }

        this._n--;
        obj.parentNode.removeChild(obj);
        document.getElementsByClassName("single_in")[this._n - 1].focus();
    }
}

function add_input(e) {
    if (e.key == "Enter") {
        state.add();
    } else if (e.target.value == "" && e.key == "Backspace") {
        state.remove(e.target);
    }
}

function handle_shortcut(e) {
    if (e.ctrlKey && e.key == " ") {
        document.getElementsByName("calculate")[0].click();
    }
}

document.getElementsByClassName("single_in")[0]
    .addEventListener("keyup", add_input);

document.getElementById("add").onclick = function(e) { state.add(); };
document.onkeyup = handle_shortcut;
