let state = {
    _n: 1,

    add: function() {
        let last = document.getElementsByClassName("pair")[this._n - 1];
        last.insertAdjacentHTML('afterend',
        "<div class='pair'>" +
        "    <input type='number' class='left'/>" +
        "    <input type='number' class='right'/>" +
        "</div>"
        )

        last = document.getElementsByClassName("pair")[this._n];
        last.addEventListener("keyup", add_input);
        last = document.getElementsByClassName("left")[this._n];
        last.focus();

        this._n++;
    },

    remove: function(obj) {
        if (this._n <= 1) { return; }

        obj.parentNode.parentNode.removeChild(obj.parentNode);
        document.getElementsByClassName("right")[--this._n - 1].focus();
    }
}

document.getElementsByClassName("left")[0].addEventListener("keyup", add_input);
document.getElementsByClassName("right")[0].addEventListener("keyup", add_input);

document.getElementById("add").onclick = function(e) { state.add(); };

function add_input(e) {
    if (e.key == "Enter") {
        state.add();
    } else if (e.target.value == "" && e.key == "Backspace") {
        state.remove(e.target);
    }
}
