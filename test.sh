#!/usr/bin/env bash
# Test all the C files for correct functionality. Has a dependency on `wasmtime`

function compile() {
    emcc "src/$1.c" src/types.c src/type_ops.c -D TEST \
    -o "/tmp/stats_sh_$1_test.wasm" 2> /dev/null
}

err=0

emcc -v

for f in src/*.c; do
    name="$(basename "$f" '.c')"
    compile "$name"
    if [[ $? != '0' ]]; then
        printf "\033[0;33m%s didn't compile. Skipping test!\033[0m\n" "$f"
        continue
    fi

    OUT="$(wasmtime "/tmp/stats_sh_${name}_test.wasm" 2>&1)"
    if [[ $? != '0' ]]; then
        printf '\033[0;31m%s failed!\033[0m\n' "$f"
        printf '%s\n' "$OUT"
        err=1
    else
        printf '\033[0;32m%s passed!\033[0m\n' "$f"
    fi
done

rm -f /tmp/stats_sh_*.wasm

exit $err
